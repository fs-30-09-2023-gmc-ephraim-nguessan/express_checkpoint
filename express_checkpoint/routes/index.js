var express = require('express');
var router = express.Router();

document.addEventListener("DOMContentLoaded", function() {
  var navLinks = document.querySelectorAll("nav ul li a");

  
  navLinks.forEach(function(link) {
      link.addEventListener("click", function(event) {
          event.preventDefault(); 
          var page = this.getAttribute("href");

          
          fetchPage(page);
      });
  });

  function fetchPage(page) {

      console.log("Chargement de la page : " + page);
  }
});


module.exports = router;
